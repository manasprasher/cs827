﻿using UnityEngine;
using System.Collections;

public class MoveTarget : MonoBehaviour {


	public GameObject pointB;
	public GameObject pointA;
	public AudioClip clip, gameOverClip;
	private AudioSource source;
	private float startTime;
	void Awake(){
		source = gameObject.AddComponent<AudioSource>();
		//source.panLevel = 0.73f;
		//source.spread = 360;
		source.clip = Resources.Load("footsteps") as AudioClip;
		source.volume = 1.0f;
		startTime = Time.time;
	}
	IEnumerator Start()
	{
	
		while (true) {
			yield return StartCoroutine(MoveObject(transform, pointA, pointB, 3.0f));
			yield return StartCoroutine(MoveObject(transform, pointB, pointA, 3.0f));
		}
     }
	
	IEnumerator MoveObject(Transform thisTransform, GameObject startPos, GameObject endPos, float time)
	{
		var i= 0.0f;
		var rate= 1.0f/time;
		while (i < 1.0f) {
			i += Time.deltaTime * rate;
			thisTransform.position = Vector3.Lerp(startPos.transform.position, endPos.transform.position, i);
			if(!source.isPlaying){
				source.Play();
			}
			yield return null; 
		}
	}
    
	//void OnMouseEnter(){
	//	print("inside");
	//	source.volume = source.volume + 0.3f;
	//	print (source.volume);
	//}

	//void OnMouseExit(){
	//	print("outside");
	//	source.volume = source.volume - 0.6f;
	//	print (source.volume);
	//}

	void onMouseOver(){
		print("over");
		source.volume = source.volume + 1.0f;
	}

	void OnGUI(){
		float guiTime = Time.time - startTime;
		int minutes = (int)guiTime / 60;
		int seconds = (int)guiTime % 60;
		float fraction = (guiTime * 100) % 100;
		print (minutes+seconds+fraction);
		string text = string.Format ("{0:00}:{1:00}:{2:00}", minutes, seconds, fraction);
		GUI.Label (new Rect (400, 25, 100, 30),text);
		//if (seconds == 5) {
		//	gameObject.SetActive(false);
		//	AudioSource.PlayClipAtPoint(gameOverClip, transform.position);
		//}
	}


}
