﻿using UnityEngine;
using System.Collections;

public class TargetVanishOnCollision : MonoBehaviour {

	public AudioClip clip;
	private AudioSource source;
	public AudioClip youWinAudio;
	private AudioSource youWinAudioSource;
	public GameObject targetObject;
	void Start () {
		source = gameObject.AddComponent<AudioSource>();
		source.clip = clip;
		youWinAudioSource = gameObject.AddComponent<AudioSource> ();
		youWinAudioSource.clip = youWinAudio;
	}
	
	// Update is called once per frame
	IEnumerator OnCollisionEnter (Collision other) {
			print ("Enemy");
			if (!source.isPlaying) {
				source.Play ();
			AudioSource.PlayClipAtPoint(youWinAudio, transform.position);
			}
			yield return  new WaitForSeconds (clip.length);

			Destroy (gameObject);

			Instantiate (targetObject, transform.position, Quaternion.identity);
		Application.LoadLevel ("Scene2");
	}
}
