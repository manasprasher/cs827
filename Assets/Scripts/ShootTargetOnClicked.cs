﻿using UnityEngine;
using System.Collections;

public class ShootTargetOnClicked : MonoBehaviour {

	public Rigidbody bullet;
	public float force;
	public ForceMode forceMode;
	public AudioClip clip;
	private AudioSource source;
	// Use this for initialization
	void Start () {
		source = gameObject.AddComponent<AudioSource>();
		source.clip = clip;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			print("shooting");
			//if(!source.isPlaying){
				source.Play();
			//}
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Vector3 spawnPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,camera.nearClipPlane));
			Rigidbody instance = Instantiate(bullet,transform.position,Quaternion.LookRotation(ray.direction)) as Rigidbody;
			instance.AddForce(ray.direction*force,forceMode);
		}
	
	}
}
