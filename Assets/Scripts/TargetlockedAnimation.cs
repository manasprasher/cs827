﻿using UnityEngine;
using System.Collections;

public class TargetlockedAnimation : MonoBehaviour {

	public RaycastHit hit;
	public GameObject target;
	public Texture image;
	private Vector3 screenloc;
	void Start () {
		print ("Dragging");
	}
	void Update(){
		if (target != null) {
			screenloc = Camera.main.WorldToScreenPoint(target.transform.position);
		}
	}
	// Update is called once per frame
	void OnGUI() {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		int layerMask = 1 << 8;
		layerMask = ~ layerMask;
			if(Physics.Raycast(ray,out hit,Mathf.Infinity,layerMask)&& target !=null){
			if(hit.collider.tag=="Enemy"){
				if(!target.audio.isPlaying){
					target.audio.Play();
				}

				GUI.DrawTexture(new Rect(screenloc.x-40,Screen.height - screenloc.y-25,image.width,image.height),image,ScaleMode.ScaleToFit,true,2.0F);
			}
	    }
	}




}
